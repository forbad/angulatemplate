import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
	images: Array<string>;
	pdfLinks: Array<string>;
	pdfPath: string;

	constructor() {
		this.images = [
			'assets/SB_Cover/SB01.jpg',
			'assets/SB_Cover/SB02.jpg',
			'assets/SB_Cover/SB03.jpg',
			'assets/SB_Cover/SB04.jpg',
			'assets/SB_Cover/SB05.jpg',
			'assets/SB_Cover/SB06.jpg',
			'assets/SB_Cover/SB07.jpg',
			'assets/SB_Cover/SB08.jpg',
			'assets/SB_Cover/SB09.jpg',
			'assets/SB_Cover/SB10.jpg',
			'assets/SB_Cover/SB11.jpg',
			'assets/SB_Cover/SB12.jpg',
			'assets/SB_Cover/SB13.jpg',
			'assets/SB_Cover/SB14.jpg',
			'assets/SB_Cover/SB15.jpg',
			'assets/SB_Cover/SB16.jpg',
			'assets/SB_Cover/SB17.jpg',
			'assets/SB_Cover/SB18.jpg',
			'assets/SB_Cover/SB19.jpg',
			'assets/SB_Cover/SB20.jpg',
			'assets/SB_Cover/SB21.jpg',
			'assets/SB_Cover/SB22.jpg',
			'assets/SB_Cover/SB23.jpg',
			'assets/SB_Cover/SB24.jpg',
			'assets/SB_Cover/SB25.jpg',
			'assets/SB_Cover/SB26.jpg',
			'assets/SB_Cover/SB27.jpg',
			'assets/SB_Cover/SB28.jpg',
			'assets/SB_Cover/SB29.jpg',
			'assets/SB_Cover/SB30.jpg',
			'assets/SB_Cover/SB31.jpg',
			'assets/SB_Cover/SB32.jpg',
			'assets/SB_Cover/SB33.jpg',
			'assets/SB_Cover/SB34.jpg',
			'assets/SB_Cover/SB35.jpg',
			'assets/SB_Cover/SB36.jpg',
			'assets/SB_Cover/SB37.jpg',
			'assets/SB_Cover/SB38.jpg',
			'assets/SB_Cover/SB39.jpg',
			'assets/SB_Cover/SB40.jpg',
			'assets/SB_Cover/SB41.jpg',
			'assets/SB_Cover/SB42.jpg',
			'assets/SB_Cover/SB43.jpg',
			'assets/SB_Cover/SB44.jpg',
			'assets/SB_Cover/SB45.jpg',
			'assets/SB_Cover/SB46.jpg',
			'assets/SB_Cover/SB47.jpg',
			'assets/SB_Cover/SB48.jpg',
			'assets/SB_Cover/SB49.jpg',
			'assets/SB_Cover/SB50.jpg'
			// 'assets/SB_Cover/SB51.jpg',
			// 'assets/SB_Cover/SB52.jpg',
			// 'assets/SB_Cover/SB53.jpg',
			// 'assets/SB_Cover/SB54.jpg',
			// 'assets/SB_Cover/SB55.jpg',
			// 'assets/SB_Cover/SB56.jpg',
			// 'assets/SB_Cover/SB57.jpg',
			// 'assets/SB_Cover/SB58.jpg',
			// 'assets/SB_Cover/SB59.jpg',
			// 'assets/SB_Cover/SB60.jpg',
			// 'assets/SB_Cover/SB61.jpg',
			// 'assets/SB_Cover/SB62.jpg',
			// 'assets/SB_Cover/SB63.jpg',
			// 'assets/SB_Cover/SB64.jpg',
			// 'assets/SB_Cover/SB65.jpg',
			// 'assets/SB_Cover/SB66.jpg',
			// 'assets/SB_Cover/SB67.jpg',
			// 'assets/SB_Cover/SB68.jpg',
			// 'assets/SB_Cover/SB69.jpg',
			// 'assets/SB_Cover/SB70.jpg',
			// 'assets/SB_Cover/SB71.jpg',
			// 'assets/SB_Cover/SB72.jpg',
			// 'assets/SB_Cover/SB73.jpg',
			// 'assets/SB_Cover/SB74.jpg',
			// 'assets/SB_Cover/SB75.jpg',
			// 'assets/SB_Cover/SB76.jpg',
			// 'assets/SB_Cover/SB77.jpg',
			// 'assets/SB_Cover/SB78.jpg',
			// 'assets/SB_Cover/SB79.jpg',
			// 'assets/SB_Cover/SB80.jpg',
			// 'assets/SB_Cover/SB81.jpg',
			// 'assets/SB_Cover/SB82.jpg',
			// 'assets/SB_Cover/SB83.jpg',
			// 'assets/SB_Cover/SB84.jpg',
			// 'assets/SB_Cover/SB85.jpg',
			// 'assets/SB_Cover/SB86.jpg',
			// 'assets/SB_Cover/SB87.jpg',
			// 'assets/SB_Cover/SB88.jpg',
			// 'assets/SB_Cover/SB89.jpg',
			// 'assets/SB_Cover/SB90.jpg',
			// 'assets/SB_Cover/SB91.jpg',
			// 'assets/SB_Cover/SB92.jpg',
			// 'assets/SB_Cover/SB93.jpg',
			// 'assets/SB_Cover/SB94.jpg',
			// 'assets/SB_Cover/SB95.jpg',
			// 'assets/SB_Cover/SB96.jpg',
			// 'assets/SB_Cover/SB97.jpg',
			// 'assets/SB_Cover/SB98.jpg',
			// 'assets/SB_Cover/SB99.jpg',
			// 'assets/SB_Cover/SB100.jpg',
			// 'assets/SB_Cover/SB101.jpg',
			// 'assets/SB_Cover/SB102.jpg',
			// 'assets/SB_Cover/SB103.jpg',
			// 'assets/SB_Cover/SB104.jpg',
			// 'assets/SB_Cover/SB105.jpg',
			// 'assets/SB_Cover/SB106.jpg',
			// 'assets/SB_Cover/SB107.jpg',
			// 'assets/SB_Cover/SB108.jpg',
			// 'assets/SB_Cover/SB109.jpg',
			// 'assets/SB_Cover/SB110.jpg'
		];
		this.pdfLinks = ['https://pdfhost.io/v/k1qUXlzl_SB51pdf.pdf'];
	}
	ngOnInit() {}

	public openPDF(message: string) {
		// let pdfPath = 'assets/pdf/' + message.substring(16);
		// pdfPath = pdfPath.slice(0, -3) + 'pdf';
		// window.open(pdfPath);
		// let div = document.getElementsByClassName('responsive_image');
		// div.innerHTML =
		// alert(pdfPath);
		let temp = message.substring(18, message.length - 4);
		this.pdfPath = this.pdfLinks[parseInt(temp) - 1];
		alert(this.pdfPath);
	}
}
