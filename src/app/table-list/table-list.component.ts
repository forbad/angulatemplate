import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
	selector: 'app-table-list',
	templateUrl: './table-list.component.html',
	styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {
	genuineUrl: SafeUrl;
	videos: Array<string> = [
		'https://www.youjizz.com/videos/embed/47067011?rel=0&amp;modestbranding=1&amp;showinfo=0',
		'https://www.youjizz.com/videos/embed/47066761',
		'https://www.youjizz.com/videos/embed/47067101',
		'https://www.youjizz.com/videos/embed/47067331',
		'https://www.youjizz.com/videos/embed/47067321',
		'https://www.youjizz.com/videos/embed/47067381',
		'https://www.youjizz.com/videos/embed/52610011',
		'https://www.youjizz.com/videos/embed/52610041',
		'https://www.youjizz.com/videos/embed/52610031',
		'https://www.youjizz.com/videos/embed/52610001'
	];
	newUrls: Array<SafeUrl> = [];
	constructor(private sanitizer: DomSanitizer) {
		this.videos.forEach(temp => {
			this.genuineUrl = this.sanitizer.bypassSecurityTrustResourceUrl(temp);
			this.newUrls.push(this.genuineUrl);
		});
	}

	ngOnInit() {}
}
