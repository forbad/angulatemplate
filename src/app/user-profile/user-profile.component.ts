import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
	selector: 'app-user-profile',
	templateUrl: './user-profile.component.html',
	styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
	genuineUrl: SafeUrl;
	videos: Array<string> = [
		'https://www.youjizz.com/videos/embed/33739521',
		'https://www.youjizz.com/videos/embed/33740711',
		'https://www.youjizz.com/videos/embed/33738981',
		'https://www.youjizz.com/videos/embed/33739121',
		'https://www.youjizz.com/videos/embed/33739321',
		'https://www.youjizz.com/videos/embed/51609451',
		'https://www.youjizz.com/videos/embed/49133691',
		'https://www.youjizz.com/videos/embed/44253841'
	];
	newUrls: Array<SafeUrl> = [];
	constructor(private sanitizer: DomSanitizer) {
		this.videos.forEach(temp => {
			this.genuineUrl = this.sanitizer.bypassSecurityTrustResourceUrl(temp);
			this.newUrls.push(this.genuineUrl);
		});
	}

	ngOnInit() {}

	public openVideo(message: string) {
		// let pdfPath = 'assets/pdf/' + message.substring(16);
		// pdfPath = pdfPath.slice(0, -3) + 'pdf';
		// // window.open(pdfPath);
		// let div = document.getElementsByClassName('responsive_image');
		// div.innerHTML =
		alert(message);
	}
}

// @Pipe({ name: 'safe' })
// export class SafePipe implements PipeTransform {
// 	constructor(private sanitizer: DomSanitizer) {}
// 	transform(url) {
// 		return this.sanitizer.bypassSecurityTrustResourceUrl(url);
// 	}
// }
